module.exports.ready = false;
module.exports.initArgs = ["helpers", "configs", "bot"]
var helpers;
var config;
var wiki
var bot;
var shell = require("shelljs")
var cowsay = require("cowsay")

String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}

exports.cowsay_p = (msg) => {
    msg.channel.send(":x: Cowsay will be back soon. Ooooooooooops.")
}

module.exports.init = (args) => {
    wiki = require("wikijs").default; //Used for r.wiki
    helpers = args[0]
    config = args[1]
    bot = args[2]
    helpers.log.send("verbose", "RANDOM module is ready.")
    module.exports.ready = true;
}

module.exports.help = (msg) => {
    var cmds = config.cmds
    var cmdsets = {}
    var string = config.general.name + " help ```"
    for (var cmd in cmds) {
        if (cmds.hasOwnProperty(cmd)) {
            if (cmdsets[cmds[cmd].handler] == undefined) {
                cmdsets[cmds[cmd].handler] = {}
            }
            cmdsets[cmds[cmd].handler][cmd] = {"name": cmd, "description": cmds[cmd].description, "usage": cmds[cmd].usage}
        }
    }
    for (var x in cmdsets) {
        if (cmdsets.hasOwnProperty(x)) {
            string = string + x + "\n"
            for (var y in cmdsets[x]) {
                if (cmdsets[x].hasOwnProperty(y)) {
                    if (!cmds[y].hide) {
                        string = string + "   " + y + " - " + cmdsets[x][y]["description"] + " - " + cmdsets[x][y]["usage"] + "\n"
                    }
                }
            }
        }
    }

    msg.author.send(string + "```")
}


module.exports.modules = (msg) => {
    var mods = config.modules
    var string = "R.Nev modules: ```"
    for (var mod in mods) {
        if (mods.hasOwnProperty(mod)) {
            string = string + mod + ": \n"
            string = string + "    Version: " + mods[mod]["version"] + "\n"
            string = string + "    Description: " + mods[mod]["description"]    + "\n"
        }
    }
    msg.channel.send(string + "```")
}

exports.stats = (msg) => {
    const embed = {
        "title": "R.Nev Stats.",
        "footer": {
          "text": "R. Nev, :bow:"
        },
        "author": {
          "name": "Robo Nev."
        },
        "fields": [
          {
            "name": "Guild Count",
            "value": "There's " + bot.guilds.size + " gowsh darn amazin servers to watch. (" + (bot.guilds.size / config.general.discordGuilds) * 100 + "%)",
            "inline": true
          },
          {
            "name": "Members",
            "value": "I gotta deal with " + bot.users.size + " amazing geezers.",
            "inline": true
          },
          {
            "name": "Modules",
            "value": helpers.load.modcount() + " Modules loaded in RAM.",
            "inline": true
          },
          {
            "name": "Uptime",
            "value": Math.round((Date.now() - helpers.startTime)/1000).toString().toHHMMSS(),
            "inline": true
          },
          {
            "name": "Version",
            "value": config.general.vnum + " (" + config.general.version + ")",
            "inline": true
          }
        ]
      };
      msg.channel.send("", { embed });
}

exports.invite = (msg) => {
    msg.reply("You can add me with: " + config.general.oauth)
}

exports.source = (msg) => {
    msg.reply("You can find the source code at " + config.general.source)

}

exports.nuke = (msg) => {
    msg.reply("You just nuked the entire world!") //Hatman added
}

exports.hats_are_dumb = (msg) => {
   msg.reply("Shut up hats are cool") //Hatman added
}

exports.wiki = (msg) => {
    if (msg.content.split(" ")[1] == undefined) {
        msg.reply(":x: Please give a query, see r.help for more information")
    }else {
        const embed = {
            "author": {
              "name": "Resolving & Parsing...",
              "icon_url": "https://upload.wikimedia.org/wikipedia/en/thumb/8/80/Wikipedia-logo-v2.svg/220px-Wikipedia-logo-v2.svg.png"
            }
          };
        msg.channel.send("", { embed }).then(message => {
            var globalPage
            helpers.log.send("verbose", "[WIKI] Looking up " + msg.content.replace("r.wiki ", ""))
            wiki().page(msg.content.replace("r.wiki ", "")).then(page => {
                globalPage = page
                helpers.log.send("verbose", "[WIKI] Found " + page["raw"]["title"])
                page.summary().then((summary) => {
                    //Exposed the summary of the page, ensure it's less than 2700 characters:
                    var data = {
                        "url": globalPage["raw"]["fullurl"],
                        "title": globalPage["raw"]["title"],
                        "ts": globalPage["raw"]["touched"], //Wat the hell?
                        "pageID": globalPage["raw"]["pageid"]
                    }
                    var embed = {
                        "url": data["url"],
                        "title": data["title"] + " (" + data["pageID"] + ")",
                        "description": "```" + summary + "```",
                        "timestamp": data["ts"],
                        "footer": {
                          "text": "Page last updated"
                        },
                        "author": {
                          "name": "Wikipedia",
                          "url": "https://wikipedia.org",
                          "icon_url": "https://upload.wikimedia.org/wikipedia/en/thumb/8/80/Wikipedia-logo-v2.svg/220px-Wikipedia-logo-v2.svg.png"
                        },
                        "fields": [
                          {
                            "name": "Note",
                            "value": "This is a summary - click the title to read the full article.   ",
                            "inline": true
                          }
                        ]
                      };
                    if (summary.length > 500) {
                        summary = summary.substring(0,500) + "..."
                        embed.description = "```" + summary + "```"
                    }
                    message.edit("Your wikipedia page, master", { embed });
                })
    
            }).catch(error => {
                const embed = {
                    "author": {
                      "name": "No article found!",
                      "icon_url": "https://upload.wikimedia.org/wikipedia/en/thumb/8/80/Wikipedia-logo-v2.svg/220px-Wikipedia-logo-v2.svg.png"
                    }
                  };
                helpers.log.send("error", "(probably not a bad error) WIKIERR: " + error)
                message.edit("Ah bugger!", { embed })
            })  
        })
    }
}

exports.changes = (msg) => {
    // git log -1 --pretty=%B - commit message
    // git rev-parse --verify HEAD - hash
    shell.exec("git log -1", (code, stdout, stderr) => {
        if (code == 1) {
            msg.reply(":x: Something went wrong getting git info - is this instance running under a git environment?")
        }else {
            const embed = {
                "title": "R. Nev Recent Changes.",
                "description": "This information comes directly from the [Git Repo](" + config.general.source + ")",
                "fields": [
                  {
                    "name": "R. Nev Versions",
                    "value": config.general.version + " (" + config.general.vnum + ")"
                  },
                  {
                    "name": "Latest git commit.",
                    "value": "```" + stdout + "```"
                  }
                ]
              };
              msg.channel.send("Latest R.Nev Changes: ", { embed });
        }
    })
}

exports.credit = (msg) => {
    var embed = {
        "title": "Credits for R. Nev Features & Codebase",
        "description": "R. Nev has features programmed or suggested by these awesome people:",
        "author": {
          "name": "R. Nev",
          "url": "https://nevexo.space",
          "icon_url": bot.user.avatarURL //Don't really know if this works.
        },
        "footer": {
            "text": "If you made a contribution and want a link adding, DM Nevexo#8002."
        },
        "fields": []
    };
    config.general.credits.forEach((item) => {
        if (item["link"] != undefined) {
            embed["fields"].push({"name": item["name"] + " - " + item["link"], "value": item["value"]})
        }else {
            embed["fields"].push({"name": item["name"], "value": item["value"]})
        }

    })
    msg.channel.send("If you want to make your own contribution to R. Nev's code base, open a PR on <" + config.general.source + ">", { embed })
}

exports.cowsay = (msg) => {
    var arg = msg.content.replace("r.cowsay ", "")
    arg = (arg + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0'); //Escape the string cause we runnin this bois.
    msg.channel.send("```" + cowsay.say({"text": arg}) + "```")
}

exports.status = (msg) => {
  var message = msg.content.replace("r.status ", "")
  var state = message.split(" | ")
  bot.user.setPresence({ game: { name: state[0]}, status: state[1] })
  .then(msg.reply(":white_check_mark: All done!"))
  .catch((err) => {
      msg.reply(":x: Something went wrong lol! " + err )
  });
}
