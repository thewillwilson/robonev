module.exports.ready = false;
module.exports.initArgs = ["helpers"]

var request;
var helpers;

module.exports.init = (args) => {
    helpers = args[0]
    config = args[1]
    bot = args[2]
    helpers.log.send("verbose", "HIBP is ready.")
    module.exports.ready = true;
    request = require("request")
}

exports.hibp = (msg) => {
  var options = {
    headers: {
      'User-Agent': 'Discord-Bot-Pwnage-check'
    }
  };
    var email = msg.content.replace("r.hibp ", "")
    request('https://haveibeenpwned.com/api/v2/breachedaccount/' + email, options, function (error, response, body) {
    if (error) {msg.reply(":x: Something went wrong")
      helpers.log.send("error" , "HIBP ERR: " + error)}else {
        if (response.statusCode != 200) {
            msg.channel.send("You haven't been pwned!")
        }else {
            body = JSON.parse(body)
            var embed = {
              "title": "You've been Pwned in " + body.length + " breaches!",
              "description": "Here's the breaches your email address was found in, this data is from the service [Have I Been Pwned](https://haveibeenpwned.com) operated by [Troy Hunt](https://troyhunt.com)",
              "url": "https://haveibeenpwned.com",
              "color": 2978794,
              "timestamp": new Date().toISOString(),
              "footer": {
                "text": "Searched conducted"
              },
              "thumbnail": {
                "url": "https://haveibeenpwned.com/Content/Images/SocialLogo.png"
              },
              "author": {
                "name": "Have I Been Pwned? (HIBP)",
                "url": "https://haveibeenpwned.com/",
                "icon_url": "https://haveibeenpwned.com/Content/Images/SocialLogo.png"
              },
              "fields": [

              ]
            };
            var limiter = 4;
            var loops = 0
            body.forEach((breach) => {
              if (loops >= limiter) {
                if (loops == limiter) {
                  embed["fields"].push({"name": "But wait, there's more... sadly.", "value": "Only " + limiter + " breaches are shown, but your account has " + body.length + ", visit [Have I Been Pwned?](https://haveibeenpwned.com) to see the rest of them!"})
                }
              }else {
                if (breach["IsVerified"]) {
                  var verified = ":white_check_mark:"
                }else {
                  var verified = ":x:"
                }
                if (breach["IsActive"]) {
                  var active = ":white_check_mark:"
                }else {
                  var active = ":x:"
                }
                var value = "Verified: " + verified + " Active: " + active + "\nBreach date: " + breach["BreachDate"] + "\nPeople Pwned: " + breach["PwnCount"] + "\nBreached Data: ```"
                breach["DataClasses"].forEach((dataClass) => {
                  value = value + dataClass + "\n"
                })  
                value = value + "```"
                if (breach["IsSpamList"]) {
                  var name = breach["Title"] + " (Spam list)" 
                }else {
                  var name = breach["Title"] + " (Account breach)"
                }
                embed["fields"].push({"name": name, "value": value})
              }
              loops++
            })
            msg.channel.send(":x: Oh no, pwned!", { embed })
        }

      }
   });
}
