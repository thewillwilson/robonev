exports.ready = false;
exports.identify = "MODMANAGER"
exports.initArgs = ["helpers", "bot", "configs", "modules"]

var helpers, bot, configs, modules;

exports.init = (args) => {
    helpers = args[0]
    bot = args[1]
    configs = args[2]
    modules = args[3]
    exports.ready = true;
}

function moduleExists(mod) {
    if (modules[mod] == undefined) {
        return false;
    }else {
        return true;
    }
}

exports.mreload = (msg) => {
    args = msg.content.split(" ")
    if (!moduleExists(args[1])) {
        msg.reply(":x: Module doesn't exist!")
    }else {
        helpers.log.toggleRecent(true) // Turn on recent logging
        msg.channel.send(":clock10: Now reloading `" + args[1] + "`")
        delete modules[args[1]]
        delete require.cache[require.resolve("../../modules/" + args[1] + "/index.js")]
        modules[args[1]] = require("../../modules/" + args[1] + "/index.js")
        //sconsole.dir(modules)
        helpers.load.initalise(modules[args[1]])
        helpers.log.toggleRecent(false)
        string = ""
        helpers.log.recent.forEach((item) => {
            string = string + item + "\n"
        })
        msg.channel.send("Reload of `" + args[1] + "` Complete. ```" + string + "```")
    }
}