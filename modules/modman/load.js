var helpers;
var bot;
var configs;
exports.loadModules = (callback) => {
    if (configs["modules"] == undefined) {
        helpers.log.send("error", "[LOADMODULES] No module version manifest.")
        process.exit(1)
    }else {
        for (var key in configs["modules"]) {
            if (configs["modules"].hasOwnProperty(key)) {
              helpers.log.send("state", "[LOAD] Loading " + key)
              if (configs.modules[key].enabled) {
                modules[key] = require("../../modules/" + key + "/index.js")
                helpers.module_list.push(key)
                //Now the module is live, runs it's init script:
                exports.initalise(modules[key])
              }

            }
        }
        callback()
    }
}

exports.initalise = (module) => {
    var args = []
    module.initArgs.forEach((req) => {
        switch(req) {
            case "helpers":
                args.push(helpers)
                break;
            case "bot": 
                args.push(bot)
                break;
            case "module_list":
                args.push(helpers.module_list)
                break;
            case "configs":
                args.push(configs)
                break;
            case "modules":
                helpers.log.send("warning", "[MOD] Module, " + module.identify + " has accessed the modules framework.")
                args.push(modules)
                break;
        }
    })
    module.init(args)
}

exports.go = (h, c, m, b, callback) => {
    helpers = h
    bot = b
    configs = c
    modules = m
    exports.loadModules(() => {
        //Give everything 2 seconds to load:
        setTimeout(()=> {
            helpers.log.send("highlight", "Checking all modules are ready...")
            for (var key in modules) {
                if (modules.hasOwnProperty(key)) {
                    if (modules[key].ready == false) {
                        helpers.log.send("error" , "Not all modules ready in time.")
                        process.exit(1)
                    }
                }
            }
            callback()
        }, 2000)
    })
}

exports.modcount = () => {
    return Object.keys(modules).length
}