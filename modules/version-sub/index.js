exports.ready = false

var helpers;
var configs, bot;
var database = require("./database.json")
var monitors = require("./monitors.json")
var fs = require("fs")
var request = require("request")
var randomstring = require("randomstring")
var validTypes = {
    "guild": ["server", "guild", "channel", "here"],
    "pm": ["pm", "dm", "message"]
}
exports.initArgs = ["helpers", "configs", "bot"]
exports.init = (args) => {
    helpers = args[0]
    configs = args[1]
    bot = args[2]
    exports.ready = true;
    helpers.log.send("state", "Submarine module is ready.")
}

function subHelp(msg) {
    const embed = {
        "title": "Version Submarine Help",
        "description": "Version Submarine is a subscription service that will PM or send messages to your channels when a new release of a service is available.",
        "color": 16312092,
        "author": {
          "name": "Version Submarine"
        },
        "fields": [
          {
            "name": "r.marine sub <service> <channel type>",
            "value": "Use this command along with a service name and server or pm (if <channel type> is left blank it will default to server)\nExample: `r.marine sub linux pm`"
          },
          {
            "name": "r.marine services",
            "value": "This command will get you a list of services and their active versions."
          },
          {
            "name": "r.marine unsub <subscription id>",
            "value": "Cancel an active subscription that you own - you can get the subscription id from r.subs\nExample: `r.unsub xxxxx`"
          },
          {
            "name": "r.marine subs",
            "value": "List of your active subscriptions (or to the guild)",
            "inline": true
          }
        ]
      };
    msg.channel.send("" , { embed })
}

function syncNow(callback, id) {
    fs.writeFile(__dirname + "/database.json", JSON.stringify(database, null, 4), (error) => {
        if (error) {
            helpers.log.send("[VS] Failed to write database " + error + " subid: " + id)
            callback(false)
        }else {
            callback(true)
        }
    })
}

function subscribe(msg, type, service_name, service) {
    helpers.log.send("verbose", "[SM] Subscribe: " + type + " to " + service["full_name"])
    var embed = {
        "title": "Operation pending...",
        "color": 16312092,
        "author": {
          "name": "Version Submarine",
          "url": "https://nevexo.space",
          "icon_url": "http://gifimage.net/wp-content/uploads/2018/05/spinner-gif-transparent-background-13.gif"
        }
    };
    msg.channel.send("Version submarine is initalising...", { embed }).then(message => {
        //Translate type
        if (validTypes["guild"].indexOf(type) >-1) {
            type = "guild"
        }else {
            type = "pm"
        }
        //Add to subscription database:
        var id = randomstring.generate(5)
        if (type == "guild") {
            var cid = msg.channel.id
        }else {
            var cid = msg.author.id
        }
        var exists = false
        database["subscriptions"].forEach(sub => {
            if (sub["channel_id"] == cid && sub["type"] == type && sub["service"] == service_name) {
                var embed = {
                    "title": "Operation failed.",
                    "description": ":x: You already have a subscription to this service. ```\nSubscription ID: "+ sub["id"] + "```",
                    "color": 16312092,
                    "author": {
                      "name": "Version Submarine",
                      "url": "https://nevexo.space",
                    }
                };
                exists = true
                message.edit("Ah frig, something aint right.", {embed})
            }
        })
        if (!exists) {
            database["subscriptions"].push(
                {
                    "id": id,
                    "type": type,
                    "service": service_name,
                    "channel_id": cid,
                    "owner": msg.author.id
                }
            )
            // Sync the database:
            syncNow((rst) => {
                if (!rst) {
                    var embed = {
                        "title": "Operation failed. Subscription ID: " + id,
                        "color": 16312092,
                        "author": {
                          "name": "Version Submarine",
                          "url": "https://nevexo.space",
                        }
                    };
                    message.edit("Ah frig, something aint right.", {embed})
                }else {
                    var embed = {
                        "title": "Task Finished.",
                        "description": ":white_check_mark: Your subscription to " + service["full_name"] + " (" + id + ") has been added. Woosh!",
                        "color": 16312092,
                        "author": {
                          "name": "Version Submarine",
                          "url": "https://nevexo.space",
                        }
                    };
                    message.edit("", {embed})
                }
            }, id)
        }
    })
}

function subList(channel, owner) {
    var rst = {"guild": [], "owner": []}
    database.subscriptions.forEach((sub) => {
        if (sub["owner"] == owner && sub["type"] == "pm") {
            rst["owner"].push({"id": sub["id"], "service": sub["service"], "service_name": monitors[sub["service"]]["full_name"]})
        }
        if (sub["channel_id"] == channel) {
            rst["guild"].push({"id": sub["id"], "service": sub["service"], "service_name": monitors[sub["service"]]["full_name"]})
        }
    })
    return rst
}

exports.submarine = (msg) => {
    if (msg.content.split(" ")[1] == "" || msg.content.split(" ")[1] == "help" || msg.content.split(" ")[1] == "h") {
        subHelp(msg)
    }else if (msg.content.split(" ")[1] == "sub") {
        if (monitors[msg.content.split(" ")[2]] == undefined) {
            msg.channel.send(":x: Invalid monitor, for a list type r.marine list.")
        }else {
            var monitor = monitors[msg.content.split(" ")[2]]
            var type = msg.content.split(" ")[3]
            if (type != undefined) {
                if (validTypes["guild"].indexOf(type) == -1 && validTypes["pm"].indexOf(type) == -1) {
                    msg.channel.send(":x: Invalid channel type, you may use: `" + validTypes["guild"] + "` or `" + validTypes["pm"] + "`")
                }else {
                    subscribe(msg, type, monitor["name"], monitor)
                }
            }else {
                subscribe(msg, "guild", monitor["name"], monitor)
            }
        }
    }else if (msg.content.split(" ")[1] == "unsub") {
        if (msg.content.split(" ")[2] == undefined) {
            msg.channel.send(":x: Please provide a subscription ID (This will change in the near future) you can find your active subscriptions with r.marine subs")
        }else {
            var found = false
            var loops = 0
            var id = msg.content.split(" ")[2]
            database["subscriptions"].forEach(sub => {
                if (sub["id"] == id && sub["owner"] == msg.author.id) {
                    found = true
                    helpers.log.send("verbose", "[VS] Removing subscription:  " + id)
                    database.subscriptions.splice(loops, 1);
                }
                loops++
            })
            if (found) {
                syncNow((rst) => {
                    if (rst) {
                        msg.channel.send(":white_check_mark: Subscription - " + id + " deleted.")
                    }else {
                        msg.channel.send(":x: Disk save failed - the subscription may not have been fully deleted.")
                    }
                })
            }else {
                msg.channel.send(":x: Couldn't find that ID, for a list run r.subs")
            }
        }
    }else if (msg.content.split(" ")[1] == "subs") {
        var subs = subList(msg.channel.id, msg.author.id)
        var owner = "```"
        var guild = "```"
        subs.owner.forEach((sub) => {
            owner = owner + "\n" + sub["id"] + " - " + sub["service"] + " - " + sub["service_name"]
        })
        subs.guild.forEach((sub) => {
            guild = guild + "\n" + sub["id"] + " - " + sub["service"] + " - " + sub["service_name"]
        })
        if (owner == "```") {
            owner = "\n`No user subscriptions`"
        }else {owner = owner + "```"}
        if (guild == "```") {
            guild = "\n`No guild subscriptions.`"
        }else {guild = guild + "```"}
        msg.channel.send("Your subscriptions:\nUser subscriptions: " + owner + "\nGuild (server) subscriptions: " + guild)
    }else if (msg.content.split(" ")[1] == "services") {
        var string = "```"
        for (var key in monitors) {
            if (monitors.hasOwnProperty(key)) {
              string = string + "\nService: " + key + "\n-> Name: " + monitors[key]["full_name"] + "\n-> Version: " + database["active_versions"][key]
            }
        }
        msg.channel.send(string + "```")
    }else {
        subHelp(msg)
    }
}

//User register shite over
//Do checks

function serviceUpdates(service, service_name, version, previous_version) {
    database.subscriptions.forEach((sub) => {
        if (sub["service"] == service) {
            const embed = {
                "title": "🎉 New Update for service: " + service_name,
                "description": "```\n" + service_name + " " + previous_version + " -> " + version + "``` ```\nSubscription: " + sub["id"] + "\nService: " + service + "```",
                "color": 16312092,
                "timestamp": new Date().toISOString(),
                "footer": {
                "text": "Update propagated "
                },
                "author": {
                "name": "Version Submarine"
                }
            };
            //Alert these peepos.
            if (sub["type"] == "guild") {
                try {
                    bot.channels.get(sub["channel_id"]).send("New version of " + service_name + " released!", { embed })
                }catch(e) {
                    helpers.log.send("error", "[VS] Guild Err: " + e)
                }
            }else {
                try {
                    bot.users.get(sub["channel_id"]).send("New version of " + service_name + " released!", { embed })
                }catch(e) {
                    helpers.log.send("error", "[VS] PM Err: " + e)
                }
            }
        }
    })
}

function runRequest(monitors, key) {
    request(monitors[key]["url"], (error, resp, body) => {
        if (error) {
            helpers.log.send("error", "[VS] Failed to GET " + key + " - " + error)
        }else if (resp.statusCode == 200) {
            //parse it based on the rules in lvo
            if (monitors[key]["json"]) {
                try {
                    body = JSON.parse(body)
                    var version = eval("body" + monitors[key]["lvo"])
                    if (version != database["active_versions"][key]) {
                        //HOLY BALLSSS 
                        helpers.log.send("state", "[VS] Service updated: " + key)
                        serviceUpdates(key, monitors[key]["full_name"], version, database["active_versions"][key])
                        database["active_versions"][key] = version
                        syncNow(() => {})
                    }else {
                        helpers.log.send("verbose", "[VS] Serivce " + key + " didn't update.")
                    }
                }catch(e) {
                    console.error(e)
                    helpers.log.send("error", "[VS] Failed to do anything: " + e)
                }
            }//Add an else for this later.
        }
    })
}

function scan() {
    for (var key in monitors) {
        if (monitors.hasOwnProperty(key)) {
            helpers.log.send("verbose", "[VS] Processing " + key)
            console.log(monitors[key]["url"])
            runRequest(monitors, key)
        }
    }
}

setInterval(() => {
    scan()
}, 1800000)
//1800000