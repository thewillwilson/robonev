exports.ready = false

var helpers;
var configs;
var request = require("request")
exports.initArgs = ["helpers", "configs"]
exports.init = (args) => {
    helpers = args[0]
    configs = args[1]
    exports.ready = true;
    helpers.log.send("state", "Moolah module is ready.")
}

exports.stocks = (msg) => {
    var arg = msg.content.replace("r.stocks ", "")
    var url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + arg + "&apikey=" + configs.token.stocks_api
    request(url, (error, response, body) => {
        if (error) {
            helpers.log.send("error", "STOCKS " + error)
            msg.reply(":x: Something went wrong, this issue has been reported.")
        }else {
            try {
                body = JSON.parse(body)
                if (body["Error Message"] != undefined) {
                    msg.reply(":x: " + body["Error Message"] + " (probably not a valid stock asset)")
                }else {
                    var stock_id = Object.keys(body["Time Series (Daily)"])[0]
                    var stock = body["Time Series (Daily)"][stock_id]
                    const embed = {
                        "title": "Stock for: " + arg,
                        "fields": [
                          {
                            "name": "Stock at open this morning",
                            "value": stock["1. open"]
                          },
                          {
                            "name": "Stock high (today)",
                            "value": stock["2. high"]
                          },
                          {
                            "name": "Stock low (today)",
                            "value": stock["3. low"]
                        },
                          {
                            "name": "Stock at close this evening",
                            "value": stock["4. close"],
                            "inline": true
                          },
                          {
                            "name": "Date",
                            "value": stock_id,
                            "inline": true
                          }
                        ]
                      };
                      msg.channel.send("Stock data for " + arg, {embed})
                }
            }catch(err) {
                helpers.log("error", err)
                msg.reply(":x: Something went wrong, this issue has been reported.")
            }
        }
    })
}