module.exports.ready = false;
module.exports.initArgs = ["helpers"]
var request = require("request")
var helpers;
module.exports.init = (args) => {
    helpers = args[0]
    helpers.log.send("verbose", "Your majesty, the APIs are ready.")
    module.exports.ready = true;
}

module.exports.api = (msg) => {
    args = msg.content.split(" ")
    if (args[1] == "tmw") {
        var start = Date.now()
        request("https://api.tmw.media/" + args[2] + "/" + args[3], {timeout: 500}, function(error, response, body) {
            if (error) {
                msg.reply(":x: Error: " + error)
            }else {
                var embed = {
                    "title": "R.Nev API manager",
                    "color": 8995573,
                    "fields": [
                      {
                        "name": "Status Code",
                        "value": response.statusCode + " - " + response.statusMessage,
                        "inline": true
                      },
                      {
                        "name": "Response Time",
                        "value": Date.now() - start + "ms",
                        "inline": true
                      },
                      {
                        "name": "Body",
                        "value": body
                      }
                    ]
                  };
                  msg.channel.send("", { embed });
            }
        })

    }else {
        msg.reply(":x: Unregistered API.")
    }
}