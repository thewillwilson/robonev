//Google Assistant module for R.Nev
//To be finished another time.
const path = require("path")
exports.ready = false;
var allowStart = true;
var fs = require("fs")

exports.initArgs = ["helpers"]
var helpers, request, assistant, log, config;
exports.init = (args) => {
    helpers = args[0]
    fs.exists(path.resolve(__dirname, 'apikey.json'), (exists) => {
        if (exists == false) {
            console.log("[WARNING] Google Assistant cannot run without it's api key. Disabled automatically.")
            allowStart = false;
        }
        if (allowStart) {
            const GoogleAssistant = require('google-assistant');
            request = require("request")
            config = {
                auth: {
                keyFilePath: path.resolve(__dirname, 'apikey.json'),
                // where you want the tokens to be saved
                // will create the directory if not already there
                savedTokensPath: path.resolve(__dirname, 'tokens.json'),
            },
            conversation: {
                lang: 'en-US', // language code for input/output (defaults to en-US)
                deviceModelId: 'xxxxxx', // Device reg
                deviceId: 'xxxxxx', // Device reg
                isNew: false, // set this to true if you want to force a new conversation and ignore the old state
                screen: {
                    isOn: true, // set this to true if you want to output results to a screen
                },
              }
            }
            assistant = new GoogleAssistant(config.auth)
            assistant.on("ready", () => {
                exports.ready = true;
                helpers.log.send("state", "Google Assistant ready.") 
            })
            assistant.on("error", (error) => {
                helpers.log.send("error", "[GASSIST] Something went wrong... " + error)
            })
        }else {
            exports.ready = true;
            helpers.log.send("state", "[GASSIST] Google Assistant disabled.")
        }
    })    

}

function runConversation(msg) {
    var startConversation = (conversaion) => {
        helpers.log.send("verbose", "Starting conversation with Google Assistant...")
        helpers.log.send("verbose", config.conversation.textQuery)
        conversaion
            .on("response", text => { log = log + "[RESPONSE] " + text + "\n"
            //msg.reply(log)
            })
            .on("screen-data", (screen) => {
                helpers.log.send("verbose", "Recieved Google Assistant Screen data " + screen)
                msg.channel.send("DEVICE SCREEN DATA ```" + JSON.stringify(screen) + "```")
            })
            .on("ended", (error, continueConversation) => {
                if (continueConversation) {
                    //Continue convo code
                    //log = log + "[FEATURE] The conversation cannot currently be continued..." + "\n"
                    msg.reply(":exclamation: Assistant has requested you continue the conversation, use r.acon <message> to continue!")
                }
                if (error) {
                    log = log + "[ERROR] Conversation ended due to error: " + error + "\n"
                }else {
                    msg.reply(":white_check_mark: Interacted with GAssistant. Log: ```" + log + "```")
                }
                
            })
    }
    assistant.start(config.conversation, startConversation);
}
exports.assistContinue = (msg) => {
    config.conversation.textQuery = msg.content.replace("r.acon", "")
    log = log + "[CONTINUE] r.acon used - Continue\n"
    config.conversation.isNew = false;
    runConversation(msg)
    
}

exports.assist = (msg) => {
    if (!allowStart) {
        msg.reply(":x: Google Assistant isn't enabled on this instance.")
    }else {
        msg.reply(":clock3: Please wait a while...")
        log = ""
        log = log + "[NEW] r.assist used - starting new conversation\n"
        config.conversation.isNew = true;
        var args = msg.content.split(" ")
        config.conversation.textQuery = msg.content.replace("r.assist", "")
        //Start the conversation
        

        runConversation(msg)
    
    }

}