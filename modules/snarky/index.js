module.exports.ready = false;
var fs = require("fs")
module.exports.initArgs = ["helpers", "configs"]
var helpers;
var configs;
module.exports.init = (args) => {
    helpers = args[0];
    configs = args[1]
    helpers.log.send("verbose", "Your majesty, the Snarky module is ready.")
    module.exports.ready = true;
}

module.exports.groundme = (msg) => {
    var obj_keys = Object.keys(configs.snark);
    var ran_key = obj_keys[Math.floor(Math.random() *obj_keys.length)];
    if (configs["servers"][msg.guild.id] != undefined)  {
        if (configs["servers"][msg.guild.id]["settings"].indexOf("DISABLE_SNARK") > -1) {
            msg.reply(":x: This is a friendly server, Snark is disabled.")
        }else {
            msg.reply(configs.snark[ran_key])
        }
    }else {
        msg.reply(configs.snark[ran_key])
    }
    //This is stupid code, oh well.
}

exports.stopSnark = (msg) => {
    helpers.log.send("state", "Disabling SNARK plugin for " + msg.guild.name)
    configs.servers[msg.guild.id] = {"settings": "DISABLE_SNARK"} 
    fs.writeFile("configs/servers.json", JSON.stringify(configs.servers), (error) => {
        if (error) {
            helpers.log.send("error", "[SNARK] Saving error: " + error)
            msg.reply("Something went wrong saving, SNARK may not be disabled.")
        }else {
            msg.reply(":white_check_mark: Snark is now disabled.")
        }
    })
}
exports.allowSnark = (msg) => {
    helpers.log.send("state", "Enabling SNARK plugin for " + msg.guild.name)
    configs.servers[msg.guild.id]["settings"] = "ENABLE_SNARK"
    fs.writeFile("configs/servers.json", JSON.stringify(configs.servers), (error) => {
        if (error) {
            helpers.log.send("error", "[SNARK] Saving error: " + error)
            msg.reply("Something went wrong saving, SNARK may not be enabled.")
        }else {
            msg.reply(":white_check_mark: Snark is now enabled.")
        }
    })
}