exports.ready = false;
var request = require("request")

exports.initArgs = ["helpers", "configs", "bot"]
var helpers;
var configs;
var bot;

exports.init = (args) => {
    helpers = args[0]
    configs = args[1]
    bot = args[2]
    exports.ready = true;
    helpers.log.send("state", "FISH module is ready.")
}

exports.fish = (msg) => {
    request(configs.general.fish_endpoint, (error, response, body) => {
        if (error) {
            helpers.log.send("error", "[FISH] Request error: " + error)
            msg.reply(":x: Something went wrong.")
        }else {
            body = JSON.parse(body)
            const embed = {
                "title": "Fish-o-meter",
                "description": "Notice: " + body.notice,
                "color": 16754761,
                "footer": {
                  "text": "This data changes every 15 minutes."
                },
                "thumbnail": {
                  "url": "https://cdn.discordapp.com/attachments/457561281935966229/457561677316096002/9J8IlG8bSSGYMEOIZCOWmlB5VKcWfSXMdJFollsQ3hUSfq_MhbNyEsf6uBezGIBLpnuR-1yPNPxjZyxxXflEFI4tnHod6_Ebt-9G.png"
                },
                "author": {
                  "name": "Robo Nev Fish Module",
                  "icon_url": "https://cdn.discordapp.com/attachments/457561281935966229/457562237842882561/unknown.png"
                },
                "fields": [
                  {
                    "name": body["tank_a"].name,
                    "value": "Last Updated: " + new Date(body["tank_a"].updated)
                  },
                  {
                    "name": "Temperature Now",
                    "value": body["tank_a"].temperature + "°c",
                    "inline": true
                  },
                  {
                    "name": "Min/Max",
                    "value": body["tank_a"].min + "/" + body["tank_a"].max + "°c",
                    "inline": true
                  },
                  {
                    "name": "Average Temperature",
                    "value": body["tank_a"].average_temp + "°c",
                    "inline": true
                  },
                  {
                    "name": "Residents",
                    "value": "Feature coming soon.",
                    "inline": true
                  },
                  {
                    "name": body["tank_b"].name,
                    "value": "Last Updated: " + new Date(body["tank_b"].updated)
                  },
                  {
                    "name": "Temperature Now",
                    "value": body["tank_b"].temperature + "°c",
                    "inline": true
                  },
                  {
                    "name": "Max/Min",
                    "value": body["tank_b"].min + "/" + body["tank_b"].max + "°c",
                    "inline": true
                  },
                  {
                    "name": "Average Temperature",
                    "value": body["tank_b"].average_temp + "°c",
                    "inline": true
                  },
                  {
                    "name": "Residents",
                    "value": "Feature coming soon.",
                    "inline": true
                  },
                  {
                    "name": "Note",
                    "value": "Both tanks should have similar temperatures as tank b is digitally synchronized with A, which uses an analouge thermostat."
                  },
                  {
                    "name": "About My Fish",
                    "value": "I have 4 adult guppys, 5 neon tetras, a catfish, 3 shrip and an uncountable number of guppys that we're trying to find a home for."
                  },
                  {
                      "name": "Upcoming features",
                      "value": "A live camera is to be added, resident tracking, electric & pH values, feeding cycle and more datapoints."
                  }
                ]
              };
              msg.channel.send("", { embed })
        }
    })
}
    exports.killfish = (msg) => {
    msg.channel.send("Your majesty, you're not allowed to kill the fishies.")
}
    exports.livefeed = (msg) => {
    msg.channel.send("Sorry, but there isn't a livefeed up atm, ask Nevexo when the livefeed will be up and if he says it's up tell him to edit fish's index.js to reflect the change")
}
