exports.ready = false;
//Fun module
//This has random "fun" things! Kinda like random, just, not!

var ytsearch
var ytsOpts 

exports.initArgs = ["helpers", "configs"]
var helpers, request;
exports.init = (args) => {
    ytsearch = require("youtube-search")
    ytsOpts = {
        "key": args[1]["token"]["youtube"],
        "maxResults": 3
    }
    helpers = args[0]
    helpers.log.send("state", "FUN module is ready.")
    request = require("request")
    exports.ready = true;
}

function runxkcd(xkcd, msg) {
    if (xkcd == "latest") {
        var url = "https://xkcd.com/info.0.json"
    }else {
        var url = "https://xkcd.com/" + xkcd + "/info.0.json"
    }
    request(url, (error, response, body) => {
            if (error) {
                msg.edit(":x: Failed to contact xkcd.")
                helpers.log.send("error", "[XKCD] Error: " + error + " - " + response)
            }else {
                if (response.statusCode != 200) {
                    msg.edit(":x: Invalid comic (or xkcd is down)")
                }else {
                    body = JSON.parse(body) //Make sure it can be formatted ezpz fair n square.
                    var posted = body["day"] + "/" + body["month"] + "/" + body["year"] + " (UK format cause fu america)"
                    const embed = {
                        "title": body["num"] + " - " + body["safe_title"],
                        "url": "https://xkcd.com/" + body["num"],
                        "color": 16777215,
                        "footer": {
                          "text": body["alt"] + " - Posted: " + posted 
                        },
                        //"timestamp": posted, //I'll add this one day...
                        "image": {
                          "url": body["img"]
                        }
                      };
                    msg.edit("Your majesty, here's your comic: ", { embed })
                }
                
            }
        })
}

exports.xkcd = (msg) => {
    //xkcd support!!! wewewew
    var args = msg.content.split(" ")
    msg.channel.send(":clock3: Finding comic...").then((msg) => {
        if (args[1] == "random") {
            request("https://c.xkcd.com/random/comic", {followAllRedirects: true}, (error, response, body) => {
                if (error) { 
                    helpers.log.send("error", "[XKCD] random error " + error)
                }else {
                    var comic = response.request.uri.href.replace("https://xkcd.com/", "")
                    comic = comic.replace("/", "")
                    runxkcd(comic, msg)
                }
            })
        }else if (args[1] == "latest"){
            runxkcd("latest", msg)
        }else {
            runxkcd(args[1], msg)
        }
    })      
}

exports.loss = (msg) => {
    msg.channel.send("I honestly have no idea what the \"loss\" meme is all about. I'm gonna like go on KnowYourMeme.")
}

exports.yts = (msg) => {
    var arg = msg.content.replace("r.yt ", "")
    ytsearch(arg, ytsOpts, (err, content) => {
        if (err) {
            helpers.log.send("error", "YTERR: " + err)
            msg.reply(":x: Something went wrong.")
        }else {
            var embed = {
                "color": 16711680,
                "footer": {
                  "text": "Results provided by YouTube API V3"
                },
                "author": {
                  "name": "YouTube",
                  "url": "https://youtube.com",
                  "icon_url": "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Frack.1.mshcdn.com%2Fmedia%2FZgkyMDEzLzA4LzI5Lzg2L3lvdXR1YmVsb2dvLjNmZmMzLmpwZwpwCXRodW1iCTk1MHg1MzQjCmUJanBn%2F334cf6a5%2Fbe5%2Fyoutube-logo.jpg&f=1"
                },
                "fields": []
              };
            content.forEach((asset)=> {
                var published = new Date(asset["publishedAt"])
                var uploaded_created = published.getUTCDate() + "/" + published.getUTCMonth() + "/" + published.getUTCFullYear()
                if (asset["kind"] == "youtube#video") {
                    var value = "By: " + asset["channelTitle"] + " \n[YouTube/" + asset["id"] + "](" + asset["link"] +") \nDescription: ```" + asset["description"] + "```\nUploaded: " + uploaded_created
                    embed["fields"].push({"name": "Video - " + asset["title"], "value": value})
                }else if (asset["kind"] == "youtube#channel") {
                    var value = "[YouTube/" + asset["id"] + "](" + asset.link + ")\nDescription: ```" + asset["description"] + "```\nCreated: " + uploaded_created
                    embed["fields"].push({"name": "Channel - " + asset["channelTitle"], "value": value})
                }
            })
            msg.channel.send("Results:", { embed })
        }
    })
}