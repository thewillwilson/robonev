module.exports.ready = false; 
var helpers;
var bot;
var config;
module.exports.initArgs = ["helpers", "configs"]
module.exports.init = (args) => {
    helpers = args[0]
    config = args[1]
    helpers.log.send("verbose", "PERMISSIONS module is ready.")
    module.exports.ready = true;
}

module.exports.verify = (author, perms, callback) => {
    var verify = false;
    perms.forEach((obj) => {
        switch(obj) {
            case "EVERYONE":
                verify = true;
                break;
            case "OWNER":
                if (author.id == config.permissions.owner) {
                    verify = true;
                }
                break;
            case "MOD": 
                if (config.permissions.mods.indexOf(author.id) > -1) {
                    verify = true;
                }
                break;
        } //No need to default that.
    })
    callback(verify)
}

module.exports.addmod = (msg) => { //TODO
    var args = msg.split(" ")
    config.permissions.mods.push(args[1])
}