//Shell plugin for R.Nev
//By Nevexo
//This script has administrative commands like update prefix, update from git or restart modules/the bot.
module.exports.ready = false;
var fs = require("fs")
var shell = require("shelljs")
var helpers;
var config;
var bot;
module.exports.initArgs = ["helpers", "configs", "bot"]
module.exports.init = function(args) {
    helpers = args[0]
    config = args[1]
    bot = args[2]
    helpers.log.send("verbose", "SHELL plugin ready.")
    module.exports.ready = true;
}

module.exports["restart"] = (msg) => {
    helpers.log.send("state", "Entering restart start.")
    config.shutdown.reason = "RESTART_COMMAND",
    config.shutdown.channel = msg.channel.id
    fs.writeFile("configs/shutdown.json", JSON.stringify(config.shutdown), (error) => {
        if (error) {
            helpers.log.send("error", "Failed to write file: " + error)
        }else {
            msg.channel.send(":clock10: Transaction Pending...")
            bot.destroy();
            process.exit(0)
        }
    })

}

module.exports.pull = (msg) => {
    //Check platform
    if (process.platform != "linux") {
        msg.reply(":x: Cannot pull on the " + process.platform + " platform.")
    }else {
        //Ensure git is installed.
        if (!shell.which('git')) {
            msg.reply(":x: Git is not installed on the target.")
        }else {
            //Okay, everything checks out. (haha get it, git meme. git good. ahah.)
            var tmp;
            msg.channel.send(":clock10: Now pulling...").then((msg) => {
                shell.exec("git pull", (code, stdout, stderr) => {
                    //msg.channel.send("DEBUG: " + code + " - " + stdout + " - " + stderr)
                    if (code == 1) {
                        msg.edit(":x: Something went wrong pulling (I assume...) \nStderr:```" + stderr + "```")
                    }else {
                        msg.edit(":white_check_mark: Git pull done. ```" + stdout + "``` \nFeel free to use r.shell restart or reload any required modules.")
                    }
                })
            })
        }

    }
    
}

module.exports.sh = (msg) => {
    helpers.log.send("verbose", msg.author.tag + " is running: " + msg.content.replace("r.sh ", "") + " via shell.")
    msg.channel.send(":clock3: Now Executing: " + msg.content.replace("r.sh ", "")).then((newMsg) => {

        if (process.platform != "linux") {
            msg.channel.send(":warning: Caution: Not running under linux!")
        }
        shell.exec(msg.content.replace("r.sh ", ""), (code, stdout, stderr) => {
            //msg.channel.send("DEBUG: " + code + " - " + stdout + " - " + stderr)
            if (code == 1) {
                newMsg.edit(":x: Something went wrong executing (I assume...) \nStderr:```" + stderr + "```")
            }else {
                newMsg.edit(":white_check_mark: All done master, ```" + stdout + "``` ")
            }
        })
    })
}