var name;
var version;
var chalk;
var enable_recent = false;

module.exports.recent = []
module.exports.toggleRecent = (state) => {
    enable_recent = state
    if (state) {
        module.exports.recent = []
        module.exports.recent.push("[RAM LOGGING] Enabled RAM logging: " + new Date())
    }
}
module.exports.init = function(n, ver, c, v, recent) {name = n, version = ver, chalk = c, 
    module.exports.send = function(type, message) {
        if (enable_recent) {
            module.exports.recent.push("[" + type.toUpperCase() + "] " + message)
        }
        var colour;
        switch(type) {
            case "state":
                colour = "bgBlue"
                break;
            case "warning": 
                colour = "bgYellow"
                break;
            case "error":
                colour = "bgRed"
                break;
            case "ok":
                colour = "bgGreen"
                break;
            case "highlight": 
                colour = "bgMagenta"
                break;
            case "verbose":
                colour = "bold"
                break;
            default:
                colour = "white"
                break;
        }
        if (type == "verbose")  {
            if (v) {console.log(chalk.grey("[" + name + " (" + version + ")" + "] ") + chalk[colour]("[" + type.toUpperCase() + "]") + " " + message)}
        }else {
            console.log(chalk.grey("[" + name + " (" + version + ")" + "] ") + chalk[colour]("[" + type.toUpperCase() + "]") + " " + message)
        }
        
    }
    }
