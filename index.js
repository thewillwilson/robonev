//Load generic configs:
var configs = {"token": require("./configs/token.json"), "snark": require("./configs/snark.json"), "permissions": require("./configs/permissions.json"), "general": require("./configs/general.json"), "modules": require("./configs/modules.json"), "cmds": require("./configs/commands.json"), "shutdown": require("./configs/shutdown.json"), "servers": require("./configs/servers.json")}
//Load generic helpers:
var helpers = {"log": require("./helpers/log.js"), "chalk": require("chalk"), "module_list": [], "load": require("./modules/modman/load.js")}
var fs = require("fs")
helpers["startTime"] = Date.now()
//Setup logging and stuff:
helpers.log.init(configs.general.short_name, configs.general.version, helpers.chalk, true) //Set to false to disable verbose.
var modules = {}; //Used to store active modules.
if (configs.shutdown.reason != "GENERAL") {
    //The shutdown reason isn't generic, enable logging
    helpers.log.toggleRecent(true);
}
helpers.log.send("state", "Hello World! " + configs.general.name + " Codename: " + configs.general.version)
//Discord.js
const Discord = require('discord.js');
const bot = new Discord.Client();
//Module load!

if (process.platform == "win32" || process.platform == "darwin") {
    helpers.log.send("warning", "Started on the " + process.platform + " platform, not all features will work!")
}

//Discord things
bot.on("ready", () => {
    helpers.log.send("ok", "Logged into Discord as " + bot.user.tag)
    if (configs.shutdown.reason == "RESTART_COMMAND") {
        helpers.log.send("state", "Shutdown by command, message transaction pending...")
        helpers.log.send("ok", "Startup logging has been disabled automatically.")
        var string = ":white_check_mark: Restart success, logs: ```"
        helpers.log.recent.forEach((item) => {
            string = string + item + "\n"
        })
        string = string + "```"
        helpers.log.toggleRecent(false) //Stop ram logging, we only wanted to log startup
        var cha = bot.channels.get(configs.shutdown.channel)
        cha.send(string)
        configs.shutdown.reason = "GENERAL"
        configs.shutdown.channel = ""
        fs.writeFile("configs/shutdown.json", JSON.stringify(configs.shutdown), (error) => {
            if (error) {
                helpers.log.send("error", "Failed to write file: " + error)
            }
            // Set the client user's presence
bot.user.setPresence({ game: { name: 'with a gun?' }, status: 'online' })
  .then(console.log)
  .catch(console.error);
        })
    }
})

//Handle messages:
bot.on("message", (msg) => {
    for (var key in configs.cmds) {
        if (configs.cmds.hasOwnProperty(key)) {
            if (msg.content.startsWith(key)) {
                //It's a command, check it's permissions:
                var cmd = configs.cmds[key]
                modules["permissions"].verify(msg.author, cmd.permissions, (state) => {
                    if (state == false) {
                        msg.reply(":no_entry_sign: Permission denied, required permissions: " + cmd.permissions)
                    }else {
                        helpers.log.send("ok", cmd.handler + "." + cmd.command_name + " Command run by " + msg.author.tag)
                        modules[cmd.handler][cmd.command_name](msg) //Execute!
                    }
                })
            }
        }
    }
})

helpers.load.go(helpers, configs, modules, bot, startBot)
//Start ALL modules.

function startBot() {
    bot.login(configs["token"]["token"])
    helpers.log.send("state", "Starting Bot!")
}

//Error catching
