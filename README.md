# R.Nev // RoboNev

R.Nev is a fairly simple Discord bot by Nevexo.

Written in Node.JS using Discord.JS - I know, and I'm not sorry.

Feel free to make issues or pull requests.

There's no actual framework docs written so if you want to make your own module just copy paste another one and you'll be able to work it out.

You can run your own version if you wish.

If you do decide to run your own version, ensure you change the owner variable in configs/permissions.json

